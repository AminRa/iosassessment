//
//  RequsetManager.swift
//  IOSAssessment
//
//  Created by AminRa on 2/31/1401 AP.
//

import Foundation
import Alamofire

class RequestManager {
    
    private var manager: Alamofire.Session
    private var token: Token!
    
    init(){
        manager = Alamofire.Session.default
        manager.session.configuration.timeoutIntervalForRequest = 10000
        manager.session.configuration.timeoutIntervalForResource = 10000
    }
    
    func requestJSONResponse<T: Decodable>(base: BaseURL, requestType: Alamofire.HTTPMethod,endpoint: Endpoint, returnType: T.Type,parametes: [String:String]=[String:String](), headers: HTTPHeaders?=nil, completion: @escaping (Decodable?)->()){
        let request = manager.request(base.rawValue + endpoint.rawValue, method: requestType, parameters: parametes, encoder: .urlEncodedForm, headers: headers, interceptor: self, requestModifier: nil)
        
        request.validate(statusCode: 200...229).responseDecodable(of: returnType) { response in
            switch response.result{
            case .success(let jsonData):
                completion(jsonData)
                break
            case .failure(let error):
                NSLog(error.localizedDescription)
                completion(nil)
                break
            }
        }
        request.resume()
    }
    
}

extension RequestManager: RequestInterceptor{
    func adapt(_ urlRequest: URLRequest, for session: Session, completion: @escaping (Result<URLRequest, Error>) -> Void) {
        completion(.success(urlRequest))
    }
    
    func retry(_ request: Request, for session: Session, dueTo error: Error, completion: @escaping (RetryResult) -> Void) {
        completion(.doNotRetry)
    }
}
