//
//  BasicToken.swift
//  IOSAssessment
//
//  Created by AminRa on 3/3/1401 AP.
//

import Foundation

class BasicToken: Token{
    override func getToken() -> String {
        return "Basic " + super.getToken()
    }
}
