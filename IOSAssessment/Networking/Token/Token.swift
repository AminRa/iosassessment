//
//  Token.swift
//  IOSAssessment
//
//  Created by AminRa on 3/3/1401 AP.
//

import Foundation

class Token{
    
    private var token = ""
    private var creationDate: Date!
    private var expirationDays: Int!
    public init(token: String, expirationDays: Int = -1){
        self.token = token
        self.creationDate = Date()
        self.expirationDays = expirationDays
    }
    
    public func getToken() -> String{
        return self.token
    }
    
    public func isExpirated() -> Bool{
        if expirationDays < 0 {
            return false
        }
        guard let expirationDate = Calendar.current.date(byAdding: .day, value: self.expirationDays, to: creationDate) else{
            return false
        }
        
        if Date() > expirationDate{
            return true
        }else{
            return false
        }
    }
    
}
