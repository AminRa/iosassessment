//
//  BearerToken.swift
//  IOSAssessment
//
//  Created by AminRa on 3/3/1401 AP.
//

import Foundation

class BearerToken: Token{
    override func getToken() -> String {
        return "Bearer " + super.getToken()
    }
}
