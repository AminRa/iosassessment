//
//  FlickerAPI.swift
//  IOSAssessment
//
//  Created by AminRa on 3/4/1401 AP.
//

import Foundation
import Alamofire

class FlickerAPI: RequestManager{
    public static let shared: FlickerAPI = FlickerAPI()
    private var requestHeaders = HTTPHeaders.default
    private let api_key = "c132e19e4bda53a2b8e46eefc1d289e7"// not a safe way to store api key
    
    private override init(){
        super.init()
        requestHeaders.add(name: "Accept", value: "application/json")
    }
    
    func getPhotosList(page: Int=1, numPerPage: Int=12,completion: @escaping ([Photo]?)->()){
        let params = [
            "method":"flickr.photos.getRecent",
            "api_key":api_key,
            "format": "json",
            "nojsoncallback": "1",
            "page": "\(page)",
            "per_page": "\(numPerPage)"
        ]
        
        requestJSONResponse(base: .fliker, requestType: .get, endpoint: .empty, returnType: RecentFlickerPageResponse.self, parametes: params, headers: requestHeaders) { response in
            guard let data = response as? RecentFlickerPageResponse else{
                completion(nil)
                return
            }
            completion(data.photos.photo)
            return
        }
    }
    
    func getPhotoSizes(photoID: String, completion: @escaping (SizesInfo?)->()){
        let params = [
            "method":"flickr.photos.getSizes",
            "api_key":api_key,
            "photo_id": photoID,
            "format": "json",
            "nojsoncallback": "1"
            ]
        requestJSONResponse(base: .fliker, requestType: .get, endpoint: .empty, returnType: SizesResponse.self, parametes: params, headers: requestHeaders) { response in
            guard let data = response as? SizesResponse else{
                completion(nil)
                return
            }
            completion(data.sizes)
            return
        }
    }
    
}
