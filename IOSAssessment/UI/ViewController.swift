//
//  ViewController.swift
//  IOSAssessment
//
//  Created by AminRa on 2/31/1401 AP.
//

import UIKit

class ViewController: UIViewController {
    var loadingView = LoadingView()
    @IBOutlet weak var collectionView: UICollectionView!
    var sizeInfos = [SizesInfo]()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        let width = self.collectionView.frame.width - 20
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: width/4, height: width/4)
        layout.minimumInteritemSpacing = 5
        DataManager.shared.perPage = Int(3 * ((self.collectionView.frame.height - 5)/(layout.itemSize.height + 5))) + 1
        collectionView.collectionViewLayout = layout
        collectionView.dataSource = self
        collectionView.delegate = self
        
        loadNextData()
        
    }
    
    func loadNextData(){
        loadingView.present(presenter: self)
        DataManager.shared.nextPage {
            self.collectionView.performBatchUpdates({
                self.collectionView.reloadSections(IndexSet(integer: 0))
            }, completion: { _ in
                self.loadingView.dismissLoadingView()
            })
            
        }
    }
}

extension ViewController: UICollectionViewDataSource, UICollectionViewDelegate{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return DataManager.shared.sizeInfos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ImageCollectionViewCell.reuseId, for: indexPath) as! ImageCollectionViewCell
        let photo = DataManager.shared.photos[indexPath.item]
        cell.loadImgFromUrl(url: DataManager.shared.sizeInfos[indexPath.item].source)
        cell.lbl1.text = "owener: " + photo.owner
        cell.lbl2.text = "title: " + photo.title
        cell.lbl3.text = "id: " + photo.id
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.item == DataManager.shared.photos.count - 5{
            loadNextData()
        }
    }
    
}
