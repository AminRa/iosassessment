//
//  UIImageView+fromURL.swift
//  IOSAssessment
//
//  Created by AminRa on 3/4/1401 AP.
//

import Foundation
import UIKit

extension UIImageView {
    public func imageFromUrl(urlString: String) {
        
        if let url = URL(string: urlString) {
            // Create Data Task
            let dataTask = URLSession.shared.dataTask(with: url) { (data, _, _) in
                DispatchQueue.main.sync {
                    if let data = data, let img = UIImage(data: data){
                        self.image = img
                    }else{
                        self.image = UIImage()
                    }
                }
            }
            dataTask.resume()
        }else{
            DispatchQueue.main.sync {
                self.image = UIImage()
            }
        }
    }
}
