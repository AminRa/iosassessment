//
//  ImageCollectionViewCell.swift
//  IOSAssessment
//
//  Created by AminRa on 3/5/1401 AP.
//

import UIKit

class ImageCollectionViewCell: UICollectionViewCell {
    public static let reuseId = "flickerImageCollectionCell"
    @IBOutlet weak var lbl1: UILabel!
    @IBOutlet weak var lbl2: UILabel!
    @IBOutlet weak var lbl3: UILabel!
    @IBOutlet var imgView: UIImageView!
    private var front = true
    override func awakeFromNib() {
        super.awakeFromNib()
        let tap = UITapGestureRecognizer()
        tap.addTarget(self, action: #selector(tapped))
        self.contentView.addGestureRecognizer(tap)
        self.imgView.contentMode = .scaleToFill
    }
    
    
   @objc func tapped(){
       
       UIView.transition(with: self.contentView, duration: 0.5, options: UIView.AnimationOptions.transitionFlipFromLeft, animations: { [weak self] () -> Void in
           guard let strongSelf = self else{
               return
           }
           strongSelf.front = !strongSelf.front
           strongSelf.imgView.isHidden = !strongSelf.front
           
        }, completion: nil)
    }
    
    func loadImgFromUrl(url: String){
        self.imgView.imageFromUrl(urlString: url)
    }
    
}
