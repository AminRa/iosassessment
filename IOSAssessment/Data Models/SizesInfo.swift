//
//  SizesInfo.swift
//  IOSAssessment
//
//  Created by AminRa on 3/4/1401 AP.
//

import Foundation

struct SizesInfo: Codable{
    var canblog: Int = 0
    var canprint: Int = 0
    var candownload: Int = 0
    var size: [PhotoInfo] = [PhotoInfo]()
    
    func getAllSizes() -> [String:PhotoInfo]{
        return Dictionary(uniqueKeysWithValues: size.map{ ($0.label, $0) })
    }
    
    private enum CodingKeys : String, CodingKey {
        case canblog = "canblog"
        case canprint = "canprint"
        case candownload = "candownload"
        case size = "size"
    }
}
