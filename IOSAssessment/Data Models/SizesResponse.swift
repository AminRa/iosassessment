//
//  SizesResponse.swift
//  IOSAssessment
//
//  Created by AminRa on 3/4/1401 AP.
//

import Foundation

struct SizesResponse: Codable{
    var sizes: SizesInfo = SizesInfo()
    var stat: String =  ""
    
    private enum CodingKeys : String, CodingKey {
        case sizes = "sizes"
        case stat = "stat"
    }
}
