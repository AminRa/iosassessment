//
//  DataManager.swift
//  IOSAssessment
//
//  Created by AminRa on 3/5/1401 AP.
//

import Foundation

class DataManager{
    
    public static let shared = DataManager()
    
    var page = 1
    var perPage = 10
    var sizeInfos = [PhotoInfo]()
    var photos = [Photo]()
    func nextPage(completion: @escaping ()->()){
        let group = DispatchGroup()
        group.enter()
        FlickerAPI.shared.getPhotosList(page: page, numPerPage: perPage) { [weak self] photos in
            if self == nil{
                group.leave()
                return
            }
            if photos?.count ?? 0 > 0{
                for (idx, photo) in photos!.enumerated(){
                    group.enter()
                    DispatchQueue.main.asyncAfter(wallDeadline: .now() + .milliseconds(idx*300)) { [weak self] in
                        if self == nil{
                            group.leave()
                            return
                        }else{
                            FlickerAPI.shared.getPhotoSizes(photoID: photo.id) { [weak self] size in
                                if self == nil{
                                    group.leave()
                                    return
                                }else{
                                    if size?.size.count ?? 0 > 0{
                                        self!.sizeInfos.append(size!.getAllSizes()["Square"]!)
                                        self!.photos.append(photo)
                                    }
                                    group.leave()
                                }
                            }
                        }
                    }
                }
            }
            group.leave()
            self!.page += 1
        }
        group.notify(queue: .main) {
            completion()
        }
    }
}
