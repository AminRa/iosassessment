//
//  RecentFlickerPage.swift
//  IOSAssessment
//
//  Created by AminRa on 3/4/1401 AP.
//

import Foundation

struct RecentFlickerPage: Codable{
    let page: Int
    let pages: Int
    let perpage: Int
    let total: Int
    let photo: [Photo]
    
    private enum CodingKeys : String, CodingKey {
        case page = "page"
        case pages = "pages"
        case perpage = "perpage"
        case total = "total"
        case photo = "photo"
    }
}
