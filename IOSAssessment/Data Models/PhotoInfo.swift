//
//  PhotoInfo.swift
//  IOSAssessment
//
//  Created by AminRa on 3/4/1401 AP.
//

import Foundation

struct PhotoInfo: Codable{
    var label: String = ""
    var width: Int = 0
    var height: Int = 0
    var source: String = ""
    var url: String = ""
    var media: String = ""
    
    private enum CodingKeys : String, CodingKey {
        case label = "label"
        case width = "width"
        case height = "height"
        case source = "source"
        case url = "url"
        case media = "media"
    }
}
