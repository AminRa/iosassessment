//
//  RecentFlickerPageResponse.swift
//  IOSAssessment
//
//  Created by AminRa on 3/4/1401 AP.
//

import Foundation

struct RecentFlickerPageResponse: Codable{
    let photos : RecentFlickerPage
    let stat: String
    private enum CodingKeys : String, CodingKey {
        case photos = "photos"
        case stat = "stat"
    }
}
